import Home from '@/views/pages/Home'

const dashboard = [
    {
        path: '/home',
        name: 'home',
        component: Home,
        meta: {
            layout: 'content',
            resource: 'Demo',
        },
    },
]

export default dashboard
