import {getField, updateField} from 'vuex-map-fields';
export default {
    namespaced: true,
    state: {
        elements: [],
        current: null,
        isCustomizerOpen: false,

        graph: null,
        paper: null
    },
    getters: {
        getField
    },
    mutations: {
        updateField
    },
    actions: {},
}
