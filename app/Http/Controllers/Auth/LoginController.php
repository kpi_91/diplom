<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\RegisterFormRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Events\Registered;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            throw ValidationException::withMessages(['email' => 'Email или пароль неверный']);

        }

        $token = Auth::user()->createToken(config('app.name'));

        return response()->json([
            'token_type' => 'Bearer',
            'token' => $token->accessToken->token,
            'user' => [
                'id' => Auth::user()->id,
                'name' => Auth::user()->name,
                'username' => Auth::user()->username,
                'avatar' => '/images/avatar-1.png',
                'email' => 'admin@materio.com',
                'role' => 'client',
            ]
        ], 200);
    }
}
